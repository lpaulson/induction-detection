## Bit of Background
The basic principle behind inductive sensing lies in the Self Resonating Frequency (SRF) of the planar coil inductor (PCI). In practice, the coil won't be a perfect inductor; parasitic capacitive and trace resistance is an unavoidable reality. Calculating the SRF is pretty straight forward. 

$` SRF = \frac{1}{2\pi C L} `$

Where C is the total capacitance, and L is the total inductance. From this, you can see that SRF can be shifted by tweaking individual parameters.

When current is applied to a wire or trace in this case, it'll induce a magnetic field around that trace. If these traces are arranged in a loop/coil, the magnetic fields will be concentrated into the center.  

When this magnetic field is brought into close proximity of a conduction object, like a copper sheet, it'll induce circular currents in the material known as Eddy currents. In the same way, the current passing through a wire generates a magnetic field, these Eddy currents create their own magnetic field that opposes the origin field; this is known as Lenz Law. This opposing field effectively reduces the inductance, shifting the SRF. This shift is what indicates the somthing conductive is near the PCI.

Inductance has two components, self-inductance and mutual inductance, which I'll discuss below.

### Mutual Inductance
I'm going to try and explain this without going too deep. As per Lens Law, when the magnetic flux from one loop (A) flows through a second loop (B),  a constant parameter proportional to the amount of magnetic flux through loop B, divided by the current in loop A is known as the mutual inductance $`M_{BA}`$.  This principle also works in reverse, making $`M_{BA} = M_{AB}`$

$` M_{BA} = \Phi_B /I_A `$

This mutual inductance is defined by the Newman formula [[1]](#ref1).

$` M_{ij} = \frac{\mu_0R_iR_i}{2} \int_{\theta=0}^{2\pi} \frac{cos\theta}{\sqrt{R_i^2+R_j^2+d^2-2R_iR_jcos\theta}}d\theta `$

Where:
+ $` R_i, R_j `$: Average radius of $`i^{th}`$ and $`j^{th}`$ loop between coil x and y, respectively
+ $` d `$: Distance between coils

I found this method to be quite computationally heavy. A 4 layer board with 15 turns per layer requires 2700 computation iterations. Furthermore, each iteration requires the function to be integrated from 0 to 2$`\pi`$. Instead, I'll use a more straightforward approximation method, which calculates a constant coupling coefficient between two inductors.

$` M_{12} = K\sqrt{L_1L_2} `$

The K approximation is defined by the following paper [[2]](#ref2)

$` K =  \frac{N^2}{0.64[(0.184x^3 - 0.525x^2 + 1.038x + 1.001)(1.67N^2 - 5.84N + 65)]} `$

Where
+ x: Distance between layers in mm
+ N: Number of turns in the layer

### Self Inductance
Because the inductor is made up of consecutive loops, the self-inductance can be explained much like the mutual inductance.
The induced magnetic flux from the first loop will pass through the subsequent loops of the inductor. According to Faradays Law, this flux will induce a current that opposes the incoming current.  When this coil is flattened into two dimensions, its self-inductance can be approximated with the equation below [[3]](#ref3)

$` L = \frac{\mu_0N^2D_{avg}C_1}{2}(ln(\frac{C_2}{\sigma})+C_3\sigma+C_4\sigma^2) `$

$` \sigma = \frac{D_{outer} - D_{inner}}{D_{outer} + D_{inner}} `$

Where:
+ $` \mu_0 `$: Free space permeability
+ $` N `$: Number of turns
+ $` D_{avg} `$: Average diameter of coil 
+ $` \sigma `$ Fill Factor
+ $` C_1, C_2, C_3, C_4 `$: Geometric coefficents [[3]](#ref3)

### Series Configuration
Applying Kirchhoff's Voltage Law to a string of Inductors in series, with the addition of the mutual inductance, yields a simple summing equation similar to a network of resistors in series. If the inductors are wound so that induced magnetic fields are in the same direction, the mutual inductances will be additive.

$` L_{eff} =  L_a + M_{ab} + L_b + M_{ba} `$

The total Effective Inductance $` L_{eff} `$ is the sum of the series Self Inductance $` L_a, L_b `$ and the Mutual Inductances $` M_a, M_b `$ 

### Parallel Configuration
In the parallel case, I find it easier to use a system of equations to define the two branch voltages.

<div  align="center">
$` V_a = L_a\frac{di_a}{dt} + M_{ab}\frac{di_b}{dt} `$
$` V_b = L_b\frac{di_b}{dt} + M_{ba}\frac{di_a}{dt} `$
</div>

The Node Voltages $` V_a, V_b `$ are the sum of voltages across Inductors $` L_a, L_b `$: and the Mutual Inductance $` M_ab, M_ba `$. Using the knowledge that $`V_a`$ and $`V_b$` are equal, and assuming the inductors are identical, we can combine and reduce the equations above to calculate the effective inductance

  $` L_{p\_eff} = \frac{L_aL_b-M^2}{L_a+L_b-2M}$`

The goal of connecting the inductors in a parallel configuration isn't to gain an increase in inductance. Instead, the PCI resistance should be dramatically decreased compared to a coil with similar physical dimensions, resulting in a more efficient drive circuit.

## Planar Coil Inductor Layout
As mentioned above, the total inductance will be made up of two parts, self-inductance and mutual inductance. To ensure the coils are correctly coupled, the coiling direction needs to switch directions on each subsequent layer. 


### Trace Resistance
The Trace resistance is reasonably straight forward. The trace length per layer is the arch length of the spiral, as the radius increase as a function of the angle. 

$` l = \int\limits_{0}^{2n\pi}{(r_0+r_{\Delta})d\theta} `$

Where:
+ $` l `$: Trace length<br>
+ $` n `$: Number of turns<br>
+ $` r_0 `$: Inner Radius<br>
+ $` r_{Delta} `$: Trace width<br>

Finally, the trace resistance can be calculated.

$` R_{DC} = \rho\frac{L}{T*W} `$

Where:
+ $` \rho `$: Resistivity (copper = 17.1E-6 [$`\Omega`$-mm/mm]<br>
+ $` l `$: Length [mm]<br>
+ $` T `$: Thickness (1oz copper 0.018-0.035mm) <br>
+ $` W `$: Width <br>

### Circuit Capacitance
Because the inductors resonant frequency is dependant on both the inductance and capacitance, a small fluctuation in an already small parasitic capacitance can result in a significant resonate frequency shift. 
Therefore, a sensor capacitor much greater than the unknown parasitic capacitance needs to be selected to minimize significant inductance changes.
 
Assume the parasitic capacitance is 1pF with a parallel sensor capacitor of 10pF, the inductors' nominal value is 10uH. If the parasitic capacitance were to fluctuate by 50%, the effective inductance would shift by about 5%. However, If a sensor capacitor of 1nF was used, a 50% change in parasitic capacitance would result in less than 100th of a percent change in the effective inductance.

| Sensor C [F] | Parastic C [F] | Nominal L [H] | Resonante F [Hz] | Equivalent L [H] | Percent Change [%] |
|:-:|:-:|:-:|:-:|:-:|:-:|
| 10.000E-12 |  1.000E-12 | 10.000E-06 |  15.175E+06 |  10.000E-06 |  0.000 |
| 10.000E-12 |  1.100E-12 | 10.000E-06 |  15.106E+06 |  10.091E-06 |  0.909 |
| 10.000E-12 |  1.500E-12 | 10.000E-06 |  14.841E+06 |  10.455E-06 |  4.545 |
| 10.000E-12 |  900.000E-15 | 10.000E-06 | 15.244E+06 | 9.909E-06 | -0.909 |
| 10.000E-12 |  500.000E-15 | 10.000E-06 |  15.532E+06 |  9.545E-06 | -4.545 |
| | | | | | |
| 1.000E-09 | 1.000E-12 | 10.000E-06 |  1.591E+06 | 10.000E-06 |  0.000 |
| 1.000E-09 | 1.100E-12 | 10.000E-06 |  1.591E+06 | 10.001E-06 |  0.010 |
| 1.000E-09 | 1.500E-12 | 10.000E-06 |  1.590E+06 | 10.005E-06 |  0.050 |
| 1.000E-09 | 900.000E-15 | 10.000E-06 |  1.591E+06 | 9.999E-06 | -0.010 |
| 1.000E-09 | 500.000E-15 | 10.000E-06 |  1.591E+06 | 9.995E-06 | -0.050 |

On the other hand, you can't just throw in 1uF capacitor in and be on your way. The more capacitance you add to the systems, the more high-frequency energy gets shunted away, lowering the bandwidth. In the figure below, at 1nF, the gain at the resonate frequency is around -1.7dB. This means that for every volt put into the system,  822mV comes out, which is very measurable. 


To better approximate the system, I will try to approximate the parasitic capacitance. Using the knowledge that there's a voltage gradient through the PCI, with the source voltage at the input and ground at the output, I'm choosing to approximate the parasitic capacitance as two long parallel plates spiralled around in a coil.

$` C = \frac{\epsilon * w * l}{d}`$

Where 
+ $`\epsilon`$ is the relative electric permeability of the FR4 material (~4.4)
+ w is the trace width
+ l is the trace length per layer 
+ d is the thickness of the FR4 material 

